﻿using SQLite4Unity3d;
using UnityEngine;

namespace Assets.scripts
{
    /// <summary>
    /// Для работы с бд
    /// SingleTon
    /// </summary>
    public class DBController
    {
        private static DBController instance; //экзэмпляр класса
        public SQLiteConnection connection; //подкллючение к бд

        /// <summary>
        /// Создание экземпляра в singleton стиле
        /// </summary>
        private DBController()
        {
            connection = new SQLiteConnection("Assets/StreamingAssets/db.db", SQLiteOpenFlags.ReadWrite);
        }

        /// <summary>
        /// Возвращение экземпляра
        /// </summary>
        /// <returns></returns>
        public static DBController GetInstance()
        {
            if(instance == null)
            {
                instance = new DBController();
            }

            return instance;
        }

        /// <summary>
        /// Перевод по коду
        /// </summary>
        /// <param name="nameCode"></param>
        /// <returns></returns>
        public string Translate(string nameCode, string area = " ")
        {
            TextTranslate labelCategory = connection.Table<TextTranslate>().Where(x => x.NameCode == nameCode && x.Area == area).FirstOrDefault();
            if(labelCategory == null)
            {
                labelCategory = new TextTranslate();
                labelCategory.NameCode = nameCode;
            }
            return labelCategory.Text();
        }
        public static string TranslateText(string nameCode, string area = "")
        {
            return GetInstance().Translate(nameCode, area);
        }
    }
}
