﻿using Assets.scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Панель с сообщением и кнопкой "Закрыть"
/// UI
/// </summary>
public class AlertController : MonoBehaviour {

    private Transform buttonClose; //кнопка "Закрыть"
    private Texture2D cursor_menu; //курсор меню
    private DBController dBController; //для работы с бд


    void Start ()
    {
        //init
        dBController = DBController.GetInstance();
        buttonClose = GetComponentsInChildren<Transform>().First(x => x.name == "Button_close");
        cursor_menu = Resources.Load<Texture2D>("media/img/cursors/cursor_menu");

        //перевод надписей
        TranslateUI();
    }

    /// <summary>
    /// Перевод надписей интерфейса
    /// </summary>
    public void TranslateUI()
    {
        buttonClose.GetComponentsInChildren<Text>().First(x => x.name == "Text_button").text = dBController.Translate("close");
    }
    

    void Update () {

        //клик
        if (Input.GetMouseButtonDown(0))
        {
            //получение объектов на пересечении с мышкой
            var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
            var elements = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pd, elements);

            foreach (RaycastResult element in elements)
            {
                //клик на "Закрыть"
                if (element.gameObject.name.Contains(buttonClose.name))
                {
                    Destroy(gameObject);
                    Cursor.SetCursor(cursor_menu, Vector2.zero, CursorMode.Auto);
                    break;
                }
            }
        }
    }
}
