﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Задаёт изменение курсора в меню со стандартного на специальный при наведении на элемент с данным классом
/// Hover логика
/// UI
/// </summary>
public class CursorPointerMenuUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Texture2D cursor_menu;
    private Texture2D cursor_menu_pointer;

    void Start ()
    {
        //загрузка курсоров
        cursor_menu = Resources.Load<Texture2D>("media/img/cursors/cursor_menu");
        cursor_menu_pointer = Resources.Load<Texture2D>("media/img/cursors/cursor_menu_pointer2");
    }
	

    /// <summary>
    /// Вход курсора мыши на объект
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        //если это не кнопка или не disabled кнопка
        if (!GetComponent<Button>() || GetComponent<Button>() && GetComponent<Button>().interactable)
        {
            Cursor.SetCursor(cursor_menu_pointer, Vector2.zero, CursorMode.Auto);
        }
    }

    /// <summary>
    /// Вывод курсора мыши с объекта
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        Cursor.SetCursor(cursor_menu, Vector2.zero, CursorMode.Auto);
    }
}
