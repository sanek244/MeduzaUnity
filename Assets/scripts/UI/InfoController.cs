﻿using Assets.scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Контроллер окна "о игре"
/// UI
/// </summary>
public class InfoController : MonoBehaviour {

    private Transform buttonClose; //кнопка "X"

    void Start()
    {
        //init
        buttonClose = GetComponentsInChildren<Transform>().First(x => x.name == "Button_close");
        var TextLeftClick = GetComponentsInChildren<Text>().First(x => x.name == "Text_control_LeftClick")
            .GetComponentsInChildren<Text>().First(x => x.name == "Text");

        //перевод надписей
        DBController dBController = DBController.GetInstance();
        GetComponentsInChildren<Text>().First(x => x.name == "label_info").text = dBController.Translate("control");
        GetComponentsInChildren<Text>().First(x => x.name == "Text_control_E")
            .GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("control_E");
        GetComponentsInChildren<Text>().First(x => x.name == "Text_control_ESC")
            .GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("control_ESC");
        GetComponentsInChildren<Text>().First(x => x.name == "Text_control_AWSD")
            .GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("control_AWSD");
        GetComponentsInChildren<Text>().First(x => x.name == "Text_control_LeftClick").text = dBController.Translate("left_mouse_button");
        TextLeftClick.text = dBController.Translate("control_LeftClick");
        TextLeftClick.rectTransform.anchoredPosition = new Vector2(TextLeftClick.rectTransform.anchoredPosition.x - 20, 0);
        GetComponentsInChildren<Text>().First(x => x.name == "Text_control_Numbers")
            .GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("control_numbers");
    }

    void Update()
    {
        //клик
        if (Input.GetMouseButtonDown(0))
        {
            //получение объектов на пересечении с мышкой
            var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
            var elements = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pd, elements);

            foreach (RaycastResult element in elements)
            {
                //клик на "X" 
                if (element.gameObject.name.Contains(buttonClose.name))
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}
