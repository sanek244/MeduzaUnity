﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Отображение интерфейса в актуальном состоянии от параметров игрока
/// </summary>
public class InterfaceController : MonoBehaviour
{
    private Gamer gamer; //игрок, состояния которого наблюдаются

    //UI
    private InventoryController inventoryUI; //UI инвентарь
    private GameObject panelMoney; //UI панель баланса
    private Text moneyUI; //UI текст баланса
    private GameObject textInteractive; //текст выбранного интерактивного элемента "[E]"


    void Start()
    {
        //init
        gamer = GameObject.Find("gamer").GetComponent<Gamer>(); //игрок
        inventoryUI = GetComponentsInChildren<InventoryController>().First(x => x.name == "Panel_inventory"); //инвентарь
        moneyUI = GetComponentsInChildren<Text>().First(x => x.name == "Text_money"); //текст баланса
        panelMoney = GetComponentsInChildren<Transform>().First(x => x.name == "Panel_money").gameObject; //панель баланса
        textInteractive = GetComponentsInChildren<Text>(true).First(x => x.name == "Text_interactive").gameObject; //текст выбранного интерактивного элемента "[E]"
    }

    void Update()
    {
        //баланс пользователя
        moneyUI.text = gamer.Money.ToString();

        //обновляем инвентаря
        inventoryUI.UpdateItems(gamer.Inventory);

        //Интерактивная надпись 
        if (gamer.InteractiveSelectedElement != null)
        {
            var size = gamer.InteractiveSelectedElement.GetComponent<SpriteRenderer>().bounds.size;
            textInteractive.GetComponent<Transform>().position = gamer.InteractiveSelectedElement.transform.position - new Vector3(size.x / 5, -size.x / 5, 0.1f);
            textInteractive.SetActive(true); //показываем
        }
        else
        {
            textInteractive.SetActive(false); //скрываем
        }

        //скрываем элементы интерфейса по настройкам (но в меню, они всегда видны)
        var menu = GameObject.Find("Menu");
        inventoryUI.gameObject.SetActive(menu || PlayerPrefs.GetInt("displaying_inventory") > 0);
        panelMoney.SetActive(menu || PlayerPrefs.GetInt("displaying_balance") > 0);
    }
}
