﻿using Assets.scripts.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Контроллер инвентаря пользователя в нижней части экрана
/// UI
/// </summary>
public class InventoryController : MonoBehaviour, IDropHandler {

    //public
    public GameObject PanelInventoryItemInit; //ячейки 

    public int SelectedPlace { get; private set; } //выбранная ячейка (нумерация с 0 идёт)


    //private
    private Gamer gamer; //игрок

    private List<Image> backgroundOfItems; //задний фон в ячейках (нужно для выделения выбраной ячейки)
    private List<Image> spritesOfItems; //изображения предметов в ячейках (нужно для смены показа содержимого ячейки)
    private Color defaultBacgroundColor; //цвет фона места по умолчанию

    //const
    private readonly string namePlace = "PlaceItem"; //название места - нужно для получения номера мест по названию 



    //*** Методы ***//
    void Start()
    {
        //init
        gamer = GameObject.Find("gamer").GetComponent<Gamer>(); //игрок
    }

    /// <summary>
    /// Инициализация под предоставленный инвентарь
    /// По ячейке на каждое место в инвентаре
    /// [вызывается изнутри Gamer в start()]
    /// </summary>
    /// <param name="items"></param>
    public void Init(Item[] items)
    {
        SelectedPlace = 0;
        spritesOfItems = new List<Image>();
        backgroundOfItems = new List<Image>();

        //очистка контейнера
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        //добавление подложек
        for (int i = 0; i < items.Length; i++)
        {
            GameObject newPlace = Instantiate(PanelInventoryItemInit, transform);
            newPlace.name = namePlace + i; //заменяем название
            spritesOfItems.Add(newPlace.GetComponentsInChildren<Image>().First(x => x.name == "Image_item"));
            backgroundOfItems.Add(newPlace.GetComponentsInChildren<Image>().First(x => x.name == "Image_background2"));
        }

        //цвет фона по умолчанию
        if(backgroundOfItems.Count > 0)
        {
            defaultBacgroundColor = backgroundOfItems[0].color;
        }

        //обновляем выбранное место
        UpdateSelectedItems();

        //обновление предметов
        UpdateItems(items);
    }

    /// <summary>
    /// Обновление отображения предметов в ячейках
    /// </summary>
    /// <param name="items"></param>
    public void UpdateItems(Item[] items)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if ((object)items[i] == null)
            {
                spritesOfItems[i].sprite = null;
                spritesOfItems[i].color = new Color(0, 0, 0, 0);
            }
            else if (spritesOfItems[i].sprite == null || spritesOfItems[i].sprite.name != items[i].NameCode)
            {
                spritesOfItems[i].sprite = Resources.Load<Sprite>(@"media/img/UI/items/" + items[i].NameCode);
                spritesOfItems[i].color = new Color(0, 0, 0, 1);
            }
        }
    }




    void Update() {

        //клик
        OnClick();

        //Нажатие кнопок
        OnKeys();
    }

    /// <summary>
    /// Возвращает номер места из названия места
    /// </summary>
    /// <param name="namePlace">название места</param>
    /// <returns></returns>
    private int GetIndexPlace(string name)
    {
        return int.Parse(name.Substring(namePlace.Length));
    }

    /// <summary>
    /// Обновляет все места, выделяя выбранное
    /// </summary>
    private void UpdateSelectedItems() {
        for(int i = 0; i < backgroundOfItems.Count; i++)
        {
            //меняем прозрачность
            backgroundOfItems[i].color = SelectedPlace == i
                ? new Color(defaultBacgroundColor.r, defaultBacgroundColor.g, defaultBacgroundColor.b, 1) 
                : defaultBacgroundColor;
        }
    }


    //*** События ***//
    /// <summary>
    /// Клик по элементу
    /// </summary>
    private void OnClick()
    {
        GameObject Menu = GameObject.Find("Menu");

        if (Menu && Input.GetMouseButtonDown(0))
        {
            //получение объектов на пересечении с мышкой
            var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
            var elements = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pd, elements);

            foreach (RaycastResult element in elements)
            {
                //клик на "место"
                if (element.gameObject.name.Contains(namePlace))
                {
                    //получаем из названия - номер места
                    SelectedPlace = GetIndexPlace(element.gameObject.name);

                    //обновляем выбранное место
                    UpdateSelectedItems();

                    break;
                }
            }
        }
    }

    /// <summary>
    /// Нажатие клавиш
    /// </summary>
    private void OnKeys()
    {
        //не работает, когда используем поисковое поле в меню (других текстовых полей нету)
        GameObject menu = GameObject.Find("Menu");
        if (menu)
        {
            var searchInput = menu.GetComponentsInChildren<InputField>().FirstOrDefault(x => x.name == "InputField_search");
            if (searchInput && searchInput.isFocused)
            {
                return;
            }
        }

        //если есть ячейки
        if (spritesOfItems.Count > 0)
        {
            //цыфры
            if (Input.GetKey(KeyCode.Alpha1)) //1
            {
                SelectedPlace = 0;
            }
            else if (Input.GetKey(KeyCode.Alpha2) && spritesOfItems.Count > 1) //2
            {
                SelectedPlace = 1;
            }
            else if (Input.GetKey(KeyCode.Alpha3) && spritesOfItems.Count > 2) //3
            {
                SelectedPlace = 2;
            }
            else if (Input.GetKey(KeyCode.Alpha4) && spritesOfItems.Count > 3) //4
            {
                SelectedPlace = 3;
            }
            else if (Input.GetKey(KeyCode.Alpha5) && spritesOfItems.Count > 4) //5
            {
                SelectedPlace = 4;
            }
            else if (Input.GetKey(KeyCode.Alpha6) && spritesOfItems.Count > 5) //6
            {
                SelectedPlace = 5;
            }
            else if (Input.GetKey(KeyCode.Alpha7) && spritesOfItems.Count > 6) //7
            {
                SelectedPlace = 6;
            }
            else if (Input.GetKey(KeyCode.Alpha8) && spritesOfItems.Count > 7) //8
            {
                SelectedPlace = 7;
            }
            else if (Input.GetKey(KeyCode.Alpha9) && spritesOfItems.Count > 8) //9
            {
                SelectedPlace = 8;
            }
            else if (Input.GetKey(KeyCode.Alpha0) && spritesOfItems.Count > 9) //0
            {
                SelectedPlace = 9;
            }
            else
            {
                return;
            }

            //обновляем выбранное место
            UpdateSelectedItems();
        }
    }

    /// <summary>
    /// Событие бросания предмета (окончание переноса)
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        //получаем перенесённый элемент
        var item = ItemDragHandler.objectDrag.transform.parent.gameObject;
        var indexItem = GetIndexPlace(item.name);
        
        //смотрим куда перенесли элемент
        bool isOut = true;

        //получение объектов на пересечении с мышкой
        var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
        var raycasts = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pd, raycasts);

        foreach (var raycast in raycasts)
        {
            var element = raycast.gameObject;

            //внутри инвентаря
            if (element.name == name)
            {
                isOut = false;
            }

            //внутрь ячейки
            if(element.name.IndexOf(namePlace) == 0 && item.name != element.name)
            {
                int indexItemB = GetIndexPlace(element.name);
                gamer.ReplaceItems(indexItem, indexItemB);
            }
        }

        //наружу инвентаря
        if (isOut)
        {
            gamer.DropItem(indexItem);
        }
    }
}
