﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Изменяет цвет текста кнопки при наведении
/// Hover логика
/// UI
/// </summary>
public class ItemButtonUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Color HoverColor = new Color(1, 0.51f, 0);

    private Color defaultColor;
    private Text text;

    void Start ()
    {
        //получение объектов
        text = GetComponentInChildren<Text>();
        defaultColor = text.color;
    }
	

    /// <summary>
    /// Вход курсора мыши на объект
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        //если кнопка не disabled
        if (GetComponent<Button>().interactable)
        {
            text.color = HoverColor;
        }
    }

    /// <summary>
    /// Вывод курсора мыши с объекта
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        text.color = defaultColor;
    }
}
