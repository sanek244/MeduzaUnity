﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.scripts.UI
{
    /// <summary>
    /// Перемещение объекта мышкой
    /// UI
    /// </summary>
    class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
    {
        public static GameObject objectDrag; //перетаскиваемый элемент
        private Vector3 offset; //смещение мышки относительно локальных координат объекта

        /// <summary>
        /// Начало перемещения
        /// </summary>
        /// <param name="eventData"></param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            //записываем смещение
            offset = transform.localPosition - Input.mousePosition;
            objectDrag = gameObject;
        }

        /// <summary>
        /// Перемещение
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            //прикрепляет объект к мышке
            transform.localPosition = Input.mousePosition + offset;
        }

        /// <summary>
        /// Конец перемещения
        /// </summary>
        /// <param name="eventData"></param>
        public void OnEndDrag(PointerEventData eventData)
        {
            //возвращаем объект обратно
            transform.localPosition = Vector3.zero;
        }
    }
}
