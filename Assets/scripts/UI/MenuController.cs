﻿using Assets.scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Контроллер окна меню + основа для других окон в меню
/// UI
/// </summary>
public class MenuController : MonoBehaviour {

    public GameObject PanelShopInit; //панель магазина
    public GameObject PanelSettingsInit; //панель настроек
    public GameObject PanelInfoInit; //панель информации о игре

    public Texture2D cursor; //курсор для меню

    private Transform buttonContinue; //кнопка "Продолжить"
    private Transform buttonShop; //кнопка "Магаззин"
    private Transform buttonSettimgs; //кнопка "Настройки"
    private Transform buttonExit; //кнопка "Выход"
    private Transform panelShop; //панель магазина
    private Transform panelSettings; //панель настроек
    private Transform panelMessage; //панель с сообщением
    private Transform panelInfo; //панель с информацией о игре
    private Transform buttonInfo; //кнопка "?"

    private DBController dBController; //для работы с бд



    void Start ()
    {
        //убираем лишнее из названия (название используется другими компоненами для быстрого поиска по сцене)
        name = name.Replace("(Clone)", "");

        //init
        dBController = DBController.GetInstance();
        buttonSettimgs = GetComponentsInChildren<Transform>(true).First(x => x.name == "Button_Settings");
        buttonShop = GetComponentsInChildren<Transform>(true).First(x => x.name == "Button_Shop");
        buttonContinue = GetComponentsInChildren<Transform>().First(x => x.name == "Button_Continue");
        buttonExit = GetComponentsInChildren<Transform>().First(x => x.name == "Button_Exit");
        panelShop = GetComponentsInChildren<Transform>().First(x => x.name == "Panel_Shop");
        panelSettings = GetComponentsInChildren<Transform>(true).First(x => x.name == "Panel_Settings");
        panelMessage = GetComponentsInChildren<Transform>(true).First(x => x.name == "Panel_Message");
        panelInfo = GetComponentsInChildren<Transform>(true).First(x => x.name == "Panel_Info");
        buttonInfo = GetComponentsInChildren<Transform>(true).First(x => x.name == "Button_info");

        //перевод надписей
        TranslateUI();

        //убираем лишнее
        buttonShop.gameObject.SetActive(false); //скрываем кнопку "Магазин"
        Destroy(panelSettings.gameObject); //панель настроек
        Destroy(panelMessage.gameObject); //панель с сообщением
        Destroy(panelInfo.gameObject); //панель с информацией о игре

        //прикреплемся к камере
        GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

        //Меняем курсор
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }

    /// <summary>
    /// Перевод надписей интерфейса
    /// </summary>
    public void TranslateUI()
    {
        GetComponentsInChildren<Text>().First(x => x.name == "label_Menu").text = dBController.Translate("menu");
        buttonContinue.GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("continue");
        buttonShop.GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("shop");
        buttonSettimgs.GetComponentsInChildren<Text>(true).First(x => x.name == "Text").text = dBController.Translate("settings");
        buttonExit.GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("exit");
    }
    


    void Update () {

        //клик
        if (Input.GetMouseButtonDown(0))
        {
            //получение объектов на пересечении с мышкой
            var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
            var elements = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pd, elements);

            foreach (RaycastResult element in elements)
            {
                //клик на "Продолжить"
                if (element.gameObject.name.Contains(buttonContinue.name))
                {
                    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
                    Cursor.visible = false;
                    Destroy(gameObject);
                    break;
                }
                //клик на "Магазин"
                else if (element.gameObject.name.Contains(buttonShop.name))
                {
                    if (panelInfo)
                    {
                        Destroy(panelInfo.gameObject);
                    }
                    GoToShop();
                    break;
                }
                //клик на "Настройки"
                else if (element.gameObject.name.Contains(buttonSettimgs.name))
                {
                    if (panelInfo)
                    {
                        Destroy(panelInfo.gameObject);
                    }
                    Destroy(panelShop.gameObject);
                    panelSettings = Instantiate(PanelSettingsInit, transform).GetComponent<Transform>();
                    panelSettings.GetComponent<SettingsController>().Menu = GetComponent<MenuController>();
                    buttonSettimgs.gameObject.SetActive(false);
                    buttonShop.gameObject.SetActive(true);
                    break;
                }
                //клик на "Выход"
                else if (element.gameObject.name.Contains(buttonExit.name))
                {
                    Application.Quit();
                    break;
                }
                //клик на "?"
                else if (element.gameObject.name.Contains(buttonInfo.name))
                {
                    if (panelInfo)
                    {
                        Destroy(panelInfo.gameObject);
                    }
                    else
                    {
                        panelInfo = Instantiate(PanelInfoInit, transform).GetComponent<Transform>();
                    }
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Переход с Настроек в Магазин
    /// </summary>
    public void GoToShop()
    {
        Destroy(panelSettings.gameObject);
        panelShop = Instantiate(PanelShopInit, transform).GetComponent<Transform>();
        buttonShop.gameObject.SetActive(false);
        buttonSettimgs.gameObject.SetActive(true);
    }
}
