﻿using Assets.scripts;
using MeduzaServer;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Контроллер окна настроек в меню
/// UI
/// </summary>
public class SettingsController : MonoBehaviour {

    public MenuController Menu; //задавать только внутри MenuController!

    private Dropdown dropDown_Languages; //выпадающий список с языками
    private Slider slider_Volume; //ползунок ообщей громкости
    private Slider slider_Sounds; //ползунок громкости звуков
    private Slider slider_Music;  //ползунок громкости музыки
    private Toggle toggle_inventory;  //чекбокс отображения интерфейса в игре
    private Toggle toggle_balance;  //чекбокс отображения баланса в игре


    void Start()
    {
        //init
        dropDown_Languages = GetComponentsInChildren<Dropdown>().First(x => x.name == "Dropdown_languages");
        slider_Volume = GetComponentsInChildren<Slider>().First(x => x.name == "Slider_volume");
        slider_Sounds = GetComponentsInChildren<Slider>().First(x => x.name == "Slider_sounds");
        slider_Music = GetComponentsInChildren<Slider>().First(x => x.name == "Slider_music");
        toggle_inventory = GetComponentsInChildren<Toggle>().First(x => x.name == "Toggle_inventory");
        toggle_balance = GetComponentsInChildren<Toggle>().First(x => x.name == "Toggle_money");

        //перевод надписей
        TranslateUI();

        //установка значений настроек
        string language = string.IsNullOrEmpty(PlayerPrefs.GetString("language")) ? TypeLanguage.DefaultLanguageString : PlayerPrefs.GetString("language");
        dropDown_Languages.value = dropDown_Languages.options.FindIndex(x => x.text == language);
        slider_Volume.value = string.IsNullOrEmpty(PlayerPrefs.GetString("volume")) ? 100 : float.Parse(PlayerPrefs.GetString("volume"));
        slider_Sounds.value = string.IsNullOrEmpty(PlayerPrefs.GetString("sounds")) ? 100 : float.Parse(PlayerPrefs.GetString("sounds"));
        slider_Music.value = string.IsNullOrEmpty(PlayerPrefs.GetString("music")) ? 100 : float.Parse(PlayerPrefs.GetString("music"));
        toggle_inventory.isOn = PlayerPrefs.GetInt("displaying_inventory") > -1;  //PlayerPrefs.GetInt("displaying_inventory") = -1 if false | 1 if true | 0 - don't set by user
        toggle_balance.isOn = PlayerPrefs.GetInt("displaying_balance") > -1; //...
    }

    /// <summary>
    /// Перевод надписей
    /// </summary>
    private void TranslateUI()
    {
        DBController dBController = DBController.GetInstance();
        GetComponentsInChildren<Text>().First(x => x.name == "label_Settings").text = dBController.Translate("settings");
        GetComponentsInChildren<Text>().First(x => x.name == "label_display").text = dBController.Translate("display_in_game");
        GetComponentsInChildren<Text>().First(x => x.name == "Text_language").text = dBController.Translate("language") + ":";
        GetComponentsInChildren<Text>().First(x => x.name == "Text_volume").text = dBController.Translate("volume") + ":";
        GetComponentsInChildren<Text>().First(x => x.name == "Text_sounds").text = dBController.Translate("sounds") + ":";
        GetComponentsInChildren<Text>().First(x => x.name == "Text_music").text = dBController.Translate("music") + ":";
        GetComponentsInChildren<Text>().First(x => x.name == "Label_display_inventory").text = dBController.Translate("inventory");
        GetComponentsInChildren<Text>().First(x => x.name == "Label_display_money").text = dBController.Translate("balance");
        
        GetComponentsInChildren<Transform>().First(x => x.name == "Button_apply")
            .GetComponentsInChildren<Text>().First(x => x.name == "Text").text = dBController.Translate("apply");
    }



    void Update()
    {
        //клик
        if (Input.GetMouseButtonDown(0))
        {
            //получение объектов на пересечении с мышкой
            var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
            var elements = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pd, elements);

            foreach (RaycastResult element in elements)
            {
                //клик на "Применить" 
                if (element.gameObject.name.Contains("Button_apply"))
                {
                    //сохранение настроек
                    PlayerPrefs.SetString("volume", slider_Volume.value.ToString());
                    PlayerPrefs.SetString("sounds", slider_Sounds.value.ToString());
                    PlayerPrefs.SetString("music", slider_Music.value.ToString());

                    PlayerPrefs.SetInt("displaying_inventory", toggle_inventory.isOn ? 1 : -1); //0 - it don't set by user
                    PlayerPrefs.SetInt("displaying_balance", toggle_balance.isOn ? 1 : -1);  //0 - it don't set by user

                    var newLanguage = dropDown_Languages.options[dropDown_Languages.value].text;
                    if (newLanguage != PlayerPrefs.GetString("language"))
                    {
                        PlayerPrefs.SetString("language", newLanguage);
                        Menu.TranslateUI(); //перевод надписей
                    }

                    //переход в магазин
                    Menu.GoToShop();
                    break;
                }
            }
        }
    }      
}
