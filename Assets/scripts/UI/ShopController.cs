﻿using Assets.scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

/// <summary>
/// Контроллер окна магазина в меню
/// UI
/// </summary>
public class ShopController : MonoBehaviour {

    public GameObject PanelMessageInit; //панель с сообщением
    public GameObject PanelSubCategoryItemInit; //кнопка подкатегорий
    public GameObject PanelCategoryItemInit; //кнопка категории 
    public GameObject PanelItemInit; //кнопка предмет

    private Transform panelCategoryItems; //контейнер категорий
    private Transform panelSubCategoryItems; //контейнер подкатегорий
    private Transform panelSubArea; //контейнер предметов и надписи
    private Transform scrollViewItems_Content; //контейнер предметов
    private Transform labelCategory; //тайтл категории и подкатегории
    private Transform inputSearch_Text; //поле ввода поиска
    //Панель с описанием выбранного элемента
    private Transform panelItemDescription_Name; //название предмета
    private Transform panelItemDescription_Description; //описание
    private Transform menu; //меню

    private DBController dBController; //для работы с бд
    private List<Category> categoryItems; //категории
    private List<SubCategory> subCategoryItems; //подкатегории
    private List<Item> items; //предметы
    private Dictionary<string, TextTranslate> namesOfItems; //названия предметов
    private Dictionary<string, TextTranslate> namesOfParameters; //названия параметров предмета
    private Dictionary<string, bool> displayParameters; //отображение параметров (показывать или нет)

    private Category selectedCategoryItem; //выбранная категория
    private SubCategory selectedSubCategoryItem; //выбранная подкатегория
    private Item selectedItem; //выбранный предмет

    private string searchText; //текст поиска

    private Gamer gamer; //игрок

    //стили 
        //катгория/подкатегория/предмет
    private static Color selectedColor = new Color(0, 105f / 255f, 1f); 
    private static Color defaultColor = new Color(0, 32f / 255f, 78f / 255f);
    private static Color hoverColor = new Color(1, 0.51f, 0);
        //кнопка покупки
    private static Color buttonBuyDisabled = new Color(0.1f, 0.1f, 0.1f);
    private static Color buttonBuyNoDisabled; //заполняется в Start()


    void Start ()
    {
        //init
        searchText = "";
        dBController = DBController.GetInstance();
        gamer = GameObject.Find("gamer").GetComponent<Gamer>();
        panelCategoryItems = GetComponentsInChildren<Transform>().First(x => x.name == "Panel_category_items");
        panelSubCategoryItems = GetComponentsInChildren<Transform>().First(x => x.name == "Panel_subcategory_items");
        panelSubArea = GetComponentsInChildren<Transform>().First(x => x.name == "Panel_sub_area");
        scrollViewItems_Content = GetComponentsInChildren<Transform>().First(x => x.name == "ScrollView_items")
            .GetComponentsInChildren<Transform>().First(x => x.name == "Content");
        labelCategory = GetComponentsInChildren<Transform>().First(x => x.name == "Text_category");
        var InputSearch = GetComponentsInChildren<Transform>().First(x => x.name == "InputField_search");
        inputSearch_Text = InputSearch.GetComponentsInChildren<Transform>().First(x => x.name == "Text");
        var PanelItemDescription = GetComponentsInChildren<Transform>().First(x => x.name == "Panel_Item_description");
        panelItemDescription_Name = PanelItemDescription.GetComponentsInChildren<Transform>().First(x => x.name == "Text_name");
        panelItemDescription_Description = PanelItemDescription.GetComponentsInChildren<Transform>().First(x => x.name == "Text_description");
        menu = GetComponentsInParent<Transform>().First(x => x.name.Contains("Menu"));

        //перевод надписей
        GetComponentsInChildren<Text>().First(x => x.name == "label_shop").text = dBController.Translate("shop");
        var InputSearch_Placeholder = InputSearch.GetComponentsInChildren<Transform>().First(x => x.name == "Placeholder");
        InputSearch_Placeholder.GetComponent<Text>().text = dBController.Translate("search") + "...";

        //получение данных из бд
        GetDBData();

        //создание категорий
        UpdateCategoryItems();

        //создание подкатегорий
        UpdateSubCategoryItems();

        //задание надписи
        UpdateText();

        //создание предметов
        UpdateItems();

        //обновление описания предмета
        UpdateItemDescription();

        //стили
        buttonBuyNoDisabled = PanelItemInit.GetComponentsInChildren<Image>().First(x => x.name == "Panel_body").color;
    }
    

    /// <summary>
    /// Получение данных из бд
    /// </summary>
    private void GetDBData()
    {
        categoryItems = dBController.connection.Table<Category>().OrderBy(x => x.Sort).ToList();
        subCategoryItems = dBController.connection.Table<SubCategory>().OrderBy(x => x.Sort).ToList();
        items = dBController.connection.Table<Item>().ToList();
        displayParameters = dBController.connection.Table<ParameterDisplay>().ToDictionary(x => x.NameCode, y => y.IsDisplay);

        //получение переводов для парамтеров
        namesOfParameters = dBController.connection.Table<TextTranslate>()
            .Where(x => x.Area == "parameter")
            .ToDictionary(x => x.NameCode);

        //получение переводов для предметов
        namesOfItems = dBController.connection.Table<TextTranslate>()
            .Where(x => x.Area == "item")
            .ToDictionary(x => x.NameCode);

        //для предметов,  которым не требуется перевод (MP5) - заполняем перевод текущим именем (что бы безопасно использовать namesOfItems без проверок)
        items.ForEach(x => {
            if (!namesOfItems.ContainsKey(x.NameCode))
            {
                namesOfItems.Add(x.NameCode, new TextTranslate(x.NameCode));
            }
        });
    }

    /// <summary>
    /// Обновление категорий (панель кнопок)
    /// </summary>
    private void UpdateCategoryItems()
    {
        //выбранная категория
        selectedCategoryItem = null;

        //очистка контейнера
        foreach (Transform child in panelCategoryItems)
        {
            Destroy(child.gameObject);
        }

        //создание категорий
        foreach (var categoryModel in categoryItems)
        {
            var categoryUI = Instantiate(PanelCategoryItemInit, panelCategoryItems);
            categoryUI.GetComponent<UnityCategory>().Id = categoryModel.Id;
            categoryUI.GetComponent<UnityCategory>().NameCode = categoryModel.NameCode;

            //замена изображения
            Sprite img = Resources.Load<Sprite>(@"media/img/UI/category-main/" + categoryModel.NameCode);
            categoryUI.GetComponentsInChildren<Image>().First(x => x.name == "Panel_img").sprite = img;
        }
    }

    /// <summary>
    /// Обновление подкатегорий (панель кнопок)
    /// </summary>
    private void UpdateSubCategoryItems()
    {
        //сброс выбранной подкатегории
        selectedSubCategoryItem = null;

        //очистка контейнера
        foreach (Transform child in panelSubCategoryItems)
        {
            Destroy(child.gameObject);
        }

        //текущие подкатегории
        var currentSubCategories = new List<SubCategory>();
        if(selectedCategoryItem != null)
        {
            currentSubCategories = subCategoryItems.Where(x => x.ParentId == selectedCategoryItem.Id).ToList();
        }

        //проверка
        Vector3 positionSubArea = panelSubArea.GetComponent<RectTransform>().anchoredPosition;
        Vector2 sizeSubArea = panelSubArea.GetComponent<RectTransform>().sizeDelta;
        if (selectedCategoryItem == null || currentSubCategories.Count == 0)
        {
            positionSubArea.y = 0f; //bottom: 0
            sizeSubArea.y = 0f;     //top: 0
            panelSubArea.GetComponent<RectTransform>().anchoredPosition = positionSubArea;
            panelSubArea.GetComponent<RectTransform>().sizeDelta = sizeSubArea;
            return;
        }
        positionSubArea.y = -17.5f;  //bottom: 0
        sizeSubArea.y = -35f;  //top: 35
        panelSubArea.GetComponent<RectTransform>().anchoredPosition = positionSubArea;
        panelSubArea.GetComponent<RectTransform>().sizeDelta = sizeSubArea;

        //создание подкатегорий для выбранной категории
        foreach (var categoryModel in currentSubCategories)
        {
            var categoryUI = Instantiate(PanelSubCategoryItemInit, panelSubCategoryItems);
            categoryUI.GetComponent<UnitySubCategory>().Id = categoryModel.Id;
            categoryUI.GetComponent<UnitySubCategory>().NameCode = categoryModel.NameCode;
            categoryUI.GetComponent<UnitySubCategory>().ParentId = categoryModel.ParentId;

            //замена изображения
            Sprite img = Resources.Load<Sprite>(@"media/img/UI/category-second/" + categoryModel.NameCode);
            categoryUI.GetComponentsInChildren<Image>().First(x => x.name == "Panel_img").sprite = img;
        }
    }

    /// <summary>
    /// Обновление надписи 
    /// </summary>
    private void UpdateText()
    {
        //очистка
        labelCategory.GetComponent<Text>().text = dBController.Translate("all_items");

        //не выбрана категория
        if (selectedCategoryItem == null)
        {
            return;
        }

        labelCategory.GetComponent<Text>().text = dBController.Translate(selectedCategoryItem.NameCode);

        //не выбрана подкатегория
        if (selectedSubCategoryItem == null)
        {
            return;
        }

        labelCategory.GetComponent<Text>().text += " -> " + dBController.Translate(selectedSubCategoryItem.NameCode);
    }

    /// <summary>
    /// Обновление предметов 
    /// </summary>
    private void UpdateItems()
    {
        //сброс выделения
        selectedItem = null;

        //очистка контейнера
        foreach (Transform child in scrollViewItems_Content)
        {
            Destroy(child.gameObject);
        }

        //фильтр предметов
        List<Item> itemsShow = items.ToList();
        if(selectedSubCategoryItem != null)
        {
            itemsShow = itemsShow.Where(x => x.SubCategoryId == selectedSubCategoryItem.Id).ToList();
        }
        else if (selectedCategoryItem != null)
        {
            itemsShow = itemsShow.Where(x => x.CategoryId == selectedCategoryItem.Id).ToList();
        }

        if(!string.IsNullOrEmpty(searchText))
        {
            itemsShow = itemsShow.Where(x => namesOfItems[x.NameCode].Text().ToLower().Contains(searchText.ToLower())).ToList();
        }


        //создание предметов
        foreach (var itemModel in itemsShow)
        {
            var itemUI = Instantiate(PanelItemInit, scrollViewItems_Content);
            itemUI.name = "Panel_item_" + itemModel.NameCode;
            itemUI.GetComponent<UnityItem>().Set(itemModel);

            //замена названия
            itemUI.GetComponentsInChildren<Text>()
                .First(x => x.name == "Text")
                .text = namesOfItems[itemModel.NameCode].Text();

            //замена цены
            itemUI.GetComponentsInChildren<Text>()
                .First(x => x.name == "Button_Text")
                .text = dBController.Translate("buy") + " " + itemModel.Price + "$";

            //замена изображения
            Sprite img = Resources.Load<Sprite>(@"media/img/UI/items/" + itemModel.NameCode);
            itemUI.GetComponentsInChildren<Image>()
                .First(x => x.name == "Image")
                .sprite = img;
        }

        //Обновление кнопок покупки предмета
        UpdateItemButtonBuy();
    }

    /// <summary>
    /// Обновление кнопок покупки предмета (если денег не ватает, то отключаем)
    /// </summary>
    private void UpdateItemButtonBuy()
    {
        foreach (Transform child in scrollViewItems_Content)
        {
            //получаем модель предмета
            var unityItem = child.gameObject.GetComponent<UnityItem>();
            Item item = new Item(unityItem);
            if (item == null)
            {
                continue;
            }

            //получаем кнопку
            Button buttonBuy = child.gameObject.GetComponentsInChildren<Button>()
                    .First(x => x.name == "Button_buy");

            //меняем disabled
            bool isDisabled = item.Price > gamer.Money;
            buttonBuy.interactable = !isDisabled;
            buttonBuy.GetComponentsInChildren<Image>()
                    .First(x => x.name == "Panel_body")
                    .color = isDisabled 
                        ? buttonBuyDisabled
                        : buttonBuyNoDisabled;
        }
    }

    /// <summary>
    /// Обновление описания предмета
    /// </summary>
    private void UpdateItemDescription()
    {
        panelItemDescription_Name.GetComponent<Text>().text = selectedItem == null ? "" : namesOfItems[selectedItem.NameCode].Text();
        panelItemDescription_Description.GetComponent<Text>().text = "";

        if (selectedItem != null) {
            string descriptionCode = selectedItem.NameCode + "_description";
            string translate = dBController.Translate(descriptionCode, "item_description");
            string description = (translate == descriptionCode ? "" : translate) + "\n";

            //параметры предмета
            List<Parameter> parametersItem = dBController.connection.Table<Parameter>().Where(x => x.ItemId == selectedItem.Id).ToList();
            foreach(Parameter parameterItem in parametersItem)
            {
                //защита от отутствия данных
                if (!displayParameters.ContainsKey(parameterItem.NameCode))
                {
                    Debug.LogError(String.Format("В таблице 'ParameterDisplay', нету записи для 'NameCode' = '{0}'", parameterItem.NameCode));
                    continue;
                }

                //показывать параметр в описании?
                if (displayParameters[parameterItem.NameCode])
                {
                    //защита от отутствия перевода
                    if (!namesOfParameters.ContainsKey(parameterItem.NameCode))
                    {
                        Debug.LogError(String.Format("В таблице 'TextTranslate', нету перевода для параметра '{0}'", parameterItem.NameCode));
                        continue;
                    }

                    //добавляем к описанию
                    description += "\n> " + namesOfParameters[parameterItem.NameCode].Text() + ": " + parameterItem.Value;
                }
            }
            
            panelItemDescription_Description.GetComponent<Text>().text = description;
        }
    }


    /// <summary>
    /// Очистка hover состояний у категорий/подкатегорий/предметов
    /// Задание динамичных стилей элементов
    /// </summary>
    private void SetStyles()
    {
        //категории и подкатегории
        GetComponentsInChildren<Transform>()
            .ToList()
            .ForEach(x => {
                //категория
                if (x.gameObject.GetComponent<UnityCategory>() != null)
                {
                    bool isSelected = selectedCategoryItem == null ? false : x.gameObject.GetComponent<UnityCategory>().Id == selectedCategoryItem.Id;

                    x.GetComponentsInChildren<Image>()
                        .First(y => y.name == "Panel_color")
                        .color = isSelected
                            ? selectedColor
                            : defaultColor;

                    x.GetComponentsInChildren<RectTransform>()
                        .First(y => y.name == "Panel_selected_wall")
                        .anchoredPosition = new Vector2(0, isSelected ? -5 : 5);
                }
                //подкатегория
                else if (x.gameObject.GetComponent<UnitySubCategory>() != null)
                {
                    bool isSelected = selectedSubCategoryItem == null ? false : x.gameObject.GetComponent<UnitySubCategory>().Id == selectedSubCategoryItem.Id;

                    x.GetComponentsInChildren<Image>()
                        .First(y => y.name == "Panel_color")
                        .color = isSelected
                            ? selectedColor
                            : defaultColor;
                }
                //предмет
                else if (x.gameObject.GetComponent<UnityItem>() != null)
                {
                    bool isSelected = selectedItem == null ? false : x.gameObject.GetComponent<UnityItem>().Id == selectedItem.Id;

                    x.GetComponent<Image>().color = isSelected
                        ? selectedColor
                        : defaultColor;
                }
            });
    }

    /// <summary>
    /// Задание hover состояний
    /// </summary>
    /// <param name="elements">Элементы интерфейса, под мышкой</param>
    private void SetHover(List<RaycastResult> elements)
    {
        foreach (RaycastResult element in elements)
        {
            //наведение на категорию
            if (element.gameObject.name.Contains("Panel_category_item") && element.gameObject.GetComponent<UnityCategory>() != null)
            {
                element.gameObject
                    .GetComponentsInChildren<Image>()
                    .First(y => y.name == "Panel_color")
                    .color = hoverColor;
                break;
            }
            //наведение на подкатегорию
            else if (element.gameObject.name.Contains("Panel_subcategory_item") && element.gameObject.GetComponent<UnitySubCategory>() != null)
            {
                element.gameObject
                    .GetComponentsInChildren<Image>()
                    .First(y => y.name == "Panel_color")
                    .color = hoverColor;
                break;
            }
            //наведение на предмет
            else if (element.gameObject.name.Contains("Panel_item") && element.gameObject.GetComponent<UnityItem>() != null)
            {
                element.gameObject.GetComponent<Image>().color = hoverColor;
                break;
            }
            //наведение на панель с сообщением
            if (element.gameObject.name.Contains("Panel_Message"))
            {
                break;
            }
        }
    }

    /// <summary>
    /// Обработка клика на элементы
    /// </summary>
    /// <param name="elements">Элементы интерфейса, под мышкой</param>
    private void OnClick(List<RaycastResult> elements)
    {
        foreach (RaycastResult element in elements)
        {
            //клик на категорию
            if (element.gameObject.name.Contains("Panel_category_item") && element.gameObject.GetComponent<UnityCategory>() != null)
            {
                var unityCategory = element.gameObject.GetComponent<UnityCategory>();
                selectedCategoryItem = selectedCategoryItem != null && selectedCategoryItem.Id == unityCategory.Id
                    ? null
                    : new Category(unityCategory);
                UpdateSubCategoryItems();
                UpdateText();
                UpdateItems();
                break;
            }
            //клик на подкатегорию
            else if (element.gameObject.name.Contains("Panel_subcategory_item") && element.gameObject.GetComponent<UnitySubCategory>() != null)
            {
                var unitySubCategory = element.gameObject.GetComponent<UnitySubCategory>();
                selectedSubCategoryItem = selectedSubCategoryItem != null && selectedSubCategoryItem.Id == unitySubCategory.Id
                    ? null
                    : new SubCategory(unitySubCategory);
                UpdateText();
                UpdateItems();
                break;
            }
            //клик на предмет
            else if (element.gameObject.name.Contains("Panel_item") && element.gameObject.GetComponent<UnityItem>() != null)
            {
                var unityItem = element.gameObject.GetComponent<UnityItem>();
                selectedItem = selectedItem != null && selectedItem.Id == unityItem.Id
                    ? null
                    : new Item(unityItem);
                UpdateItemDescription();
                break;
            }
            //клик на кнопку покупки предмета
            else if (element.gameObject.name.Contains("Button_buy") && element.gameObject.GetComponent<Button>().interactable)
            {
                OnClickBuyItem(element.gameObject);
            }
            //клик на панель с сообщением
            if (element.gameObject.name.Contains("Panel_Message"))
            {
                break;
            }
        }
    }

    /// <summary>
    /// Обработка клика на кнопку покупки предмета
    /// </summary>
    /// <param name="buttonBuyItem"></param>
    private void OnClickBuyItem(GameObject buttonBuyItem)
    {
        var unityItem = buttonBuyItem.GetComponentInParent<UnityItem>();

        if(gamer.Money < unityItem.Price)
        {
            //выводим сообщение, что нехватает денег
            var PanelMessage = Instantiate(PanelMessageInit, menu);
            PanelMessage.GetComponentInChildren<Text>().text = dBController.Translate("message_not_money", "alert");
            return;
        }

        if (gamer.BuyItem(new Item(unityItem)) == false)
        {
            //выводим сообщение, что инвентарь полон и говорим как его освободить
            var PanelMessage = Instantiate(PanelMessageInit, menu);
            PanelMessage.GetComponentInChildren<Text>().text = dBController.Translate("message_not_place_in_inventory", "alert");
            return;
        }
        else
        {
            //обновляем состояние кнопок покупки у предметов
            UpdateItemButtonBuy();
        }
    }


    void Update () {
        
        //получение объектов на пересечении с мышкой
        var pd = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pd, results);

        //клик
        if (Input.GetMouseButtonDown(0))
        {
            //обработка клика на элементы
            OnClick(results);
        }

        //задание динамичных стилей
        SetStyles();

        //hover элемент
        SetHover(results);

        //Поиск
        if(inputSearch_Text.GetComponent<Text>().text != searchText)
        {
            searchText = inputSearch_Text.GetComponent<Text>().text;
            UpdateItems();
        }
    }
}
