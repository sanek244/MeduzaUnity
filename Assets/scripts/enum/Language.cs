﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum Language {
        EN, //Английский
        RU, //Русский
        ZH //Китайский
    };

    public static class TypeLanguage
    {
        public static Dictionary<Language, string> Values = new Dictionary<Language, string>()
        {
            { Language.EN, "English" },
            { Language.RU, "Russian" },
            { Language.ZH, "Chinese" },
        };
        public static Dictionary<string, Language> ValuesReverse = new Dictionary<string, Language>()
        {
            { "English", Language.EN },
            { "Russian", Language.RU },
            { "Chinese", Language.ZH },
        };

        public static Language DefaultLanguage = Language.EN; //язык по умолчанию
        public static string DefaultLanguageString = Values[DefaultLanguage]; 
    }
}