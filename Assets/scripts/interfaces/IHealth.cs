﻿
namespace MeduzaServer
{
    public interface IHealth
    {
        float Health { get; set; }
        int HealthStart { get; set; }
        float Armor { get; set; }
        TypeMaterial Material { get; set; }
    }
}
