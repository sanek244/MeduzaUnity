﻿
namespace Assets.scripts.interfaces.ui
{
    public interface ICategory
    {
        int Id { get; set; }
        string NameCode { get; set; } //ключ перевода названия
        int Sort { get; set; } //порядок сортировки при показе
    }
}
