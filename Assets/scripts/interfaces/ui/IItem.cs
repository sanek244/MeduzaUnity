﻿
namespace Assets.scripts.interfaces.ui
{
    public interface IItem
    {
        int Id { get; set; }
        int CategoryId { get; set; } //id категории
        int SubCategoryId { get; set; } //id подкатегории
        int Price { get; set; } //цена
        string NameCode { get; set; } //ключ перевода названия
    }
}
