﻿
namespace Assets.scripts.interfaces.ui
{
    public interface ISubCategory
    {
        int Id { get; set; }
        int ParentId { get; set; } //id категории
        string NameCode { get; set; } //ключ перевода названия
        int Sort { get; set; } //порядок сортировки при показе
    }
}
