﻿using MeduzaServer;
using UnityEngine;

/// <summary>
/// Строения
/// </summary>
public class Building : Essence {

    //для Unity
    /// <summary>
    /// Уровни урона объекту (первый объект - изначальный, целый)
    /// </summary>
    public Sprite[] damages;

    //*** Методы ***//
    void Start ()
    {
        Material = TypeMaterial.Concrete;
    }

    void Update()
    {
        //если есть уровни повреждений, то по ним показываем спрайты
        if(damages != null) {
            int currentFrame = (int)(damages.Length - Health / (HealthStart / damages.Length));
            gameObject.GetComponent<SpriteRenderer>().sprite = damages[currentFrame];
        }
    }
}
