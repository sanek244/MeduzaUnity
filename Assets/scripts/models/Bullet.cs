﻿using MeduzaServer;
using UnityEngine;

/// <summary>
/// Пуля
/// </summary>
public class Bullet : MonoBehaviour
{
    //*** Свойства ***//
    /// <summary>
    /// Скорость перемещения 
    /// </summary>
    public float Speed { get; set; }
    /// <summary>
    /// Дальность полёта (зависит от мощности оружия и от самой пули)
    /// </summary>
    public float ShotRange { get; set; }
    /// <summary>
    /// Бронебойность
    /// </summary>
    public float ArmorDamage { get; set; }
    /// <summary>
    /// Урон (зависит от мощности оружия и начального урона самой пули)
    /// </summary>
    public float Damage { get; set; }

    private float Dx { get; set; }
    private float Dy { get; set; }
    private Vector3 positionStart;


    //*** Методы ***//
    void Start()
    {
        Speed = 40; //по умолчанию

        //рассчёт dx, dy
        var secondPoint = Helper.GetAngleRightTriangle(transform.position, transform.eulerAngles.z, 1f);
        Dx = secondPoint.x - transform.position.x;
        Dy = secondPoint.y - transform.position.y;

        //начальное положение пули
        positionStart = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    void Update()
    {
        //место назначения за это обновление
        var newPosition = new Vector3(
            transform.position.x + Dx * Speed * Time.deltaTime,
            transform.position.y + Dy * Speed * Time.deltaTime,
            transform.position.z);

        //ищем попадания по объектам с жизнями
        var hit = Physics2D.Raycast(transform.position, new Vector2(Dx, Dy), Speed * Time.deltaTime, LayerMask.GetMask("life"));
        if (hit.collider != null) {
            hit.collider.gameObject.GetComponent<Essence>().Health -= Damage;

            //создание анимации попадания
            var material = hit.collider.gameObject.GetComponent<Essence>().Material.ToString().ToLower();
            var contactAnimator = Resources.Load<RuntimeAnimatorController>("media/img/bullet/bullet_contacts/" + material + "/" + material);
            if (contactAnimator != null) {
                var contact = new GameObject();
                contact.AddComponent<SpriteRenderer>();
                contact.AddComponent<Animator>();
                contact.GetComponent<Animator>().runtimeAnimatorController = contactAnimator;
                contact.transform.position = hit.point;
                contact.transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z + 90);
            }

            //уничтожение пули
            Destroy(gameObject);
            return;
        }

        //проверка на дальность полёта
        var distance = Helper.Distance(positionStart, newPosition);
        if (distance > ShotRange / 100) {
            Destroy(gameObject);
            return;
        }

        //меняем позицию
        transform.position = newPosition;
    }
}
