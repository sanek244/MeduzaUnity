﻿using UnityEngine;
using MeduzaServer;
using System.Linq;
using System.Collections.Generic;
using System;

/// <summary>
/// Объект игрока и базовые вещи отображающиеся для каждого игрока (меню, интерфейс, магазин, курсор)
/// </summary>
public class Gamer : Essence
{
    //Init prefabs
    public GameObject bottomInit; //нижняя часть (ноги)
    public GameObject weaponInit; //оружие
    public GameObject menuInit; //меню
    public GameObject itemDiscardedInit; //выкинутый из инвентаря предмет

    public Texture2D cursorNone; //прозрачный курсор, что бы убрать баг не исчезающего курсора после меню

    //UI
    private InventoryController inventoryUI; //UI инвентарь

    //other
    private Sprite[] scatters; //спрайты отдачи от автомата 
    private Sprite spriteInit; //начальный спрайт игрока (может заменяться)

    //интерактивные элементы
    private List<GameObject> interactiveElements; //Интерактивные элементы в радиусе действия
    public GameObject InteractiveSelectedElement { get; private set; } //Выбранный интерактивный элементы в радиусе действия

    //связанные элементы
    private GameObject[] cursor_parts; //игровой курсор
    private GameObject menu; //созданное меню
    private GameObject bottom; //созданная нижняя часть (ноги)
    private GameObject selectedItem; //созданный выбранный предмет
    private Action selectedItemOnKeys; //обработчик нажатий клавиш от выбранного предмета

    private bool isMenuWasOpen; //меню было открыто при предыдущем обновлении?

    //Параметры
    public string Nickname { get; private set; } //Никнейм игрока
    public float Speed { get; private set; } //скорость движения
    public int Force { get; private set; } //сила - влияет на разброс
    public int Money { get; private set; } //игровые деньги
    public int Score { get; private set; } //индивидуальный счёт
    public bool IsMoving { get; private set; } //движется?
    public Item[] Inventory { get; private set; } //инвентарь
    private float recoil; //отдача


    //*** Свойства ***//
    /// <summary>
    /// Отдача (0 - нет, 1 - максимум)
    /// </summary>
    public float Recoil {
        get { return recoil; }
        set
        {
            if(value > 1) {
                recoil = 1;
            }
            else if(value < 0) {
                recoil = 0;
            }
            else {
                recoil = value;
            }

            int indexScatters = Mathf.RoundToInt((recoil / (1f / scatters.Length))) - 1;

            //скин отдачи
            gameObject.GetComponent<SpriteRenderer>().sprite = indexScatters > -1 
                ? scatters[indexScatters] 
                : spriteInit;
        }
    }
    /// <summary>
    /// Количество свободных ячеек в инвентаре
    /// </summary>
    /// <returns></returns>
    public int CountFreePlacesInInventory
    {
        get {
            return Inventory.Where(x => (object)x == null).Count();
        }
    }


    //*** Методы ***//
    void Start () {

        //начальные значения
        Nickname = "GamerTest";
        Money = 5000;
        Speed = 2.5f;
        Force = 1;
        Material = TypeMaterial.Bio;
        Inventory = new Item[10];
        spriteInit = gameObject.GetComponent<SpriteRenderer>().sprite;
        interactiveElements = new List<GameObject>();
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
        selectedItemOnKeys = () => { };

        //спрайты отдачи
        scatters = new Sprite[5];
        for (int i = 0; i < 5; i++)
        {
            scatters[i] = Resources.Load<Sprite>("media/img/gamers/warrior/warrior_scatter_" + (i + 1));
        }

        //Ноги
        bottom = Instantiate(bottomInit, transform);
        bottom.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);

        //Курсор
        float sizeCursorParts = 0.3f;
        cursor_parts = new GameObject[5];
        for(int i = 0; i < cursor_parts.Length; i++) {
            cursor_parts[i] = new GameObject();
            cursor_parts[i].name = "Cursot_part" + (i + 1);
            cursor_parts[i].AddComponent<SpriteRenderer>();
            cursor_parts[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("media/img/cursors/cursor_part" + (i + 1));
            cursor_parts[i].GetComponent<SpriteRenderer>().sortingOrder = 9;
            if (i == cursor_parts.Length - 1) {
                sizeCursorParts = 0.2f;
            }
            cursor_parts[i].transform.localScale = new Vector3(sizeCursorParts, sizeCursorParts, sizeCursorParts);
        }
        Cursor.visible = false;

        //init UI инвентаря
        inventoryUI = GameObject.Find("Interface").GetComponentsInChildren<InventoryController>().FirstOrDefault(x => x.name == "Panel_inventory");
        if (inventoryUI)
        {
            inventoryUI.Init(Inventory);
        }
    }
	
	void Update ()
    {
        //камера за игроком
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);

        //Нажатие кнопок
        OnKeys();


        //если меню открыто, то игнорируем управление персонажем
        if (menu == null)
        {
            //Поворот игрока за мышкой
            RotateGamer();

            //Перемещаем и вращаем выбранный предмет вслед за игроком 
            RotateAndMoveSelectedItem();

            //Поворот нижней части тела
            RotateBottom();

            //Перемещение курсора
            CursorMove();

            //Показ кнопки взаимодействия с интерактивными элементами
            ViewKeyInteractive();

            //если было открыто меню в предыдущее обновление
            if (isMenuWasOpen)
            {
                //изменяем выбранный предмет в руках
                ChangeSelectedItem();
            }
        }

        //Уменьшение отдачи
        if (Recoil > 0) {
            Recoil -= 1.5f * Force * Time.deltaTime;
        }

        //запоминаем открытое меню
        isMenuWasOpen = menu != null;
    }


    /* прикрёпленные элементы */
    /// <summary>
    /// Поворот игрока за мышкой
    /// </summary>
    private void RotateGamer()
    {
        Vector3 mouseOisitionScene = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
        float rotateX = Helper.GetRotateAngle(GetComponent<Transform>().position, mouseOisitionScene);

        transform.eulerAngles = new Vector3(0, 0, rotateX);
    }

    /// <summary>
    /// Изменение положения и вращение выбранного предмета вслед за игроком
    /// </summary>
    private void RotateAndMoveSelectedItem()
    {
        if (selectedItem == null)
        {
            return;
        }

        //set rotate
        selectedItem.transform.eulerAngles = transform.eulerAngles;

        //alias
        var gamerLocation = transform.position;

        //точка появления предмета (со сдвигом относительно игрока)
        var positionCreateItem = new Vector3(
            gamerLocation.x + 0.2f - Recoil / 10f,
            gamerLocation.y - 0.1f,
            gamerLocation.z);
        //расстояние от игрока до предмета
        float distanceGamerForItem = Helper.Distance(positionCreateItem, gamerLocation);
        //угол между игроком и предметом
        float rotateCreatedItem = Helper.GetRotateAngle(gamerLocation, positionCreateItem);
        //конечная точка предмета
        var positionItemRotate = Helper.GetAngleRightTriangle(gamerLocation, transform.eulerAngles.z + rotateCreatedItem, distanceGamerForItem);
        //set location
        selectedItem.transform.position = new Vector3(positionItemRotate.x, positionItemRotate.y, positionItemRotate.z - 1);

        /*
            //Маним-шаманим, оружие на место ставим
            weapon.X(el.X() + el.pointCreateItemXY[0] - el.scatter.CurrentFrame() * 2 + 2 + weapon.img.size[0] / 2);
            weapon.Y(el.Y() + el.pointCreateItemXY[1] + weapon.img.size[1] / 2);
            var r = Helper.Distance(weapon.X(), weapon.Y(), el.X(), el.Y());
            var Angle0 = Math.acos((weapon.X() - el.X()) / r) * 180 / Math.PI;
            var XY = Helper.CoordInRightTriangle(el.X(), el.Y(), Angle0 + el.RotateAngle(), r);
            weapon.X(XY[0]);
            weapon.Y(XY[1]);*/
    }

    /// <summary>
    /// Поворот нижней части тела
    /// </summary>
    private void RotateBottom()
    {
        if (bottom == null) {
            return;
        }

        //Если по часовой стрелки угол считается, то k = 0; Иначе k = 90;
        int k = 90;

        //передача параметра аниматору
        bottom.GetComponent<Animator>().SetBool("walk", IsMoving);

        //Поворот
        if (Input.GetKey(KeyCode.W)) 
        {
            if (Input.GetKey(KeyCode.D)) 
            {
                if (transform.eulerAngles.z > 45 - k && transform.eulerAngles.z < 225 - k) {
                    ChangeBottom(135 - k);
                }
                else {
                    ChangeBottom(315 - k);
                }
            }
            else if (Input.GetKey(KeyCode.A)) 
            {
                if (transform.eulerAngles.z < 315 - k && transform.eulerAngles.z > 135 - k) {
                    ChangeBottom(225 - k);
                }
                else {
                    ChangeBottom(45 - k);
                }
            }
            else {
                if (transform.eulerAngles.z >= 0 && transform.eulerAngles.z < 180) {
                    ChangeBottom(90);
                }
                else {
                    ChangeBottom(270);
                }
            }
        }
        else if (Input.GetKey(KeyCode.S)) 
        {
            if (Input.GetKey(KeyCode.D))
            {
                if (transform.eulerAngles.z <= 135 - k || transform.eulerAngles.z > 315 - k) {
                    ChangeBottom(45 - k);
                }
                else {
                    ChangeBottom(225 - k);
                }
            }
            else if (Input.GetKey(KeyCode.A)) 
            {
                if (transform.eulerAngles.z > 45 - k && transform.eulerAngles.z < 225 - k) {
                    ChangeBottom(135 - k);
                }
                else {
                    ChangeBottom(315 - k);
                }
            }
            else {
                if (transform.eulerAngles.z > 0 && transform.eulerAngles.z < 180) {
                    ChangeBottom(90);
                }
                else {
                    ChangeBottom(270);
                }
            }
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (transform.eulerAngles.z > 90 && transform.eulerAngles.z < 270) {
                ChangeBottom(180);
            }
            else {
                ChangeBottom(0);
            }
        }
        else if (Input.GetKey(KeyCode.A)) 
        {
            if (transform.eulerAngles.z < 90 || transform.eulerAngles.z > 270) {
                ChangeBottom(0);
            }
            else {
                ChangeBottom(180);
            }
        }
    }

    /// <summary>
    /// Изменение ног
    /// </summary>
    /// <param name="rotateAngleBottom"></param>
    private void ChangeBottom(float rotateAngleBottom)
    {
        bottom.GetComponent<Transform>().eulerAngles = new Vector3(0, 0, rotateAngleBottom);
    }

    /// <summary>
    /// Перемещение курсора
    /// </summary>
    private void CursorMove()
    {
        //смещение частей курсора
        float kof = 0.1f * (Recoil + 1f);
        //Смещение всего курсора
        float offsetY = -0.1f;

        Vector3 mouseOisitionScene = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
        cursor_parts[0].transform.position = new Vector3(mouseOisitionScene.x - kof, mouseOisitionScene.y + kof + offsetY, mouseOisitionScene.z);
        cursor_parts[1].transform.position = new Vector3(mouseOisitionScene.x + kof, mouseOisitionScene.y + kof + offsetY, mouseOisitionScene.z);
        cursor_parts[2].transform.position = new Vector3(mouseOisitionScene.x + kof, mouseOisitionScene.y - kof + offsetY, mouseOisitionScene.z);
        cursor_parts[3].transform.position = new Vector3(mouseOisitionScene.x - kof, mouseOisitionScene.y - kof + offsetY, mouseOisitionScene.z);
        cursor_parts[4].transform.position = new Vector3(mouseOisitionScene.x, mouseOisitionScene.y + offsetY, mouseOisitionScene.z);
    }



    /* Инвентарь */
    /// <summary>
    /// Добавить предмет в инвентарь
    /// </summary>
    /// <param name="item"></param>
    /// <returns>Удалось добавить? если нет - значит инвентарь полон</returns>
    private bool AddItem(Item item)
    {
        //если привязан UI инвентаря и в нём выделена пустая ячейка
        if (inventoryUI && inventoryUI.SelectedPlace > -1 && (object)Inventory[inventoryUI.SelectedPlace] == null)
        {
            Inventory[inventoryUI.SelectedPlace] = item;
            return true;
        }

        //поиск свободного места слева
        for(int i = 0; i < Inventory.Length; i++)
        {
            if ((object)Inventory[i] == null)
            {
                //обновление инвентаря
                Inventory[i] = item;
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Удалить предмет из инвентаря
    /// </summary>
    /// <param name="indexItem"></param>
    /// <returns></returns>
    private void RemoveItem(int indexItem)
    {
        Inventory[indexItem] = null;
    }

    /// <summary>
    /// Выбросить предмет из инвентаря
    /// </summary>
    /// <param name="indexItem"></param>
    public void DropItem(int indexItem)
    {
        //получаем выкидываемый предмет
        Item itemDrop = Inventory[indexItem];

        //удаляем из инвентаря
        RemoveItem(indexItem);

        //создаём предмет на карте
        var itemDiscarded = Instantiate(itemDiscardedInit);
        itemDiscarded.GetComponent<Transform>().position = transform.position;
        itemDiscarded.GetComponent<Transform>().rotation = transform.rotation;
        itemDiscarded.GetComponent<ItemDiscarded>().Set(itemDrop);
        itemDiscarded.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(@"media/img/UI/items/" + itemDrop.NameCode);
    }

    /// <summary>
    /// Поменять местами предметы в инвентаре
    /// </summary>
    /// <param name="indexItemA"></param>
    /// <param name="indexItemB"></param>
    public void ReplaceItems(int indexItemA, int indexItemB)
    {
        //меняем местами
        var ItemA = Inventory[indexItemA];
        Inventory[indexItemA] = Inventory[indexItemB];
        Inventory[indexItemB] = ItemA;
    }
    
    /// <summary>
    /// Купить предмет
    /// </summary>
    /// <param name="item"></param>
    /// <returns>Удалось купить? если нет - значит инвентарь полон или нехватает денег</returns>
    public bool BuyItem(Item item)
    {
        if (Money < item.Price)
        {
            return false;
        }

        if (AddItem(item) == false) 
        {
            return false;
        }

        //обновление баланса
        Money -= item.Price;
        return true;
    }

    /// <summary>
    /// Изменение selectedItem на новое из инвентаря по индексу inventoryUI.SelectedPlace
    /// </summary>
    private void ChangeSelectedItem()
    {
        //ячейки не выбраны
        if(inventoryUI.SelectedPlace < 0)
        {
            return;
        }

        var newSelectedItem = Inventory[inventoryUI.SelectedPlace];
        
        //если старый выбранный предмет существует
        if (selectedItem != null)
        {
            //если уже выбран этот предмет
            if (newSelectedItem != null && selectedItem.GetComponent<UnityItem>().Id == newSelectedItem.Id)
            {
                return;
            }

            //уничтожаем его
            Destroy(selectedItem);
            selectedItemOnKeys = () => { };
        }

        //если выбран новый предмет 
        if(newSelectedItem != null)
        {
            //создаём новый оьъект
            selectedItem = Instantiate(new GameObject(), transform);
            selectedItem.name = "SelectedItem__" + Nickname + "__" + newSelectedItem.NameCode; 
            selectedItem.AddComponent<UnityItem>();
            selectedItem.GetComponent<UnityItem>().Set(newSelectedItem);

            //устанавливаем его параметры по классу
            string className = newSelectedItem.GetParameterValue("class");
            switch (className)
            {
                case "weapon":
                    selectedItem.AddComponent<SpriteRenderer>();
                    selectedItem.AddComponent<Weapon>();
                    selectedItem.GetComponent<Weapon>().Init(newSelectedItem);
                    selectedItemOnKeys = selectedItem.GetComponent<Weapon>().OnKeys;
                    break;
            }

            //вращение и перемещение выбранного предмета
            RotateAndMoveSelectedItem();
        }
    }

    /* Другое */
    /// <summary>
    /// Показывать кнопку взаимодействия с интерактивными элементами
    /// </summary>
    private void ViewKeyInteractive()
    {
        List<GameObject> interactiveElementsFiltered = interactiveElements.ToList();

        //инвентарь полон
        if (CountFreePlacesInInventory == 0)
        {
            //убираем предметы (их некуда брать)
            interactiveElementsFiltered = interactiveElementsFiltered.Where(x => x.GetComponent<ItemDiscarded>() == null).ToList();
        }

        //нет пригодных элементов в радиусе действия
        if (interactiveElementsFiltered.Count == 0)
        {
            InteractiveSelectedElement = null;
            return;
        }

        //выбор одного элемента, с которым можно будет совершить взаимодействие
        InteractiveSelectedElement = interactiveElementsFiltered[0];
        if(interactiveElementsFiltered.Count > 1)
        {
            //поиск наиближайшего
            float minDistance = -1;
            foreach(var element in interactiveElementsFiltered)
            {
                float distance = Vector3.Distance(element.transform.position, transform.position);
                if(minDistance < 0 || minDistance > distance)
                {
                    minDistance = distance;
                    InteractiveSelectedElement = element;
                }
            }
        }
    }


    

    //*** События ***//
    /// <summary>
    /// Нажатие клавиш
    /// </summary>
    private void OnKeys()
    {
        //Меню
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (menu == null)
            {
                //показать
                menu = Instantiate(menuInit);
                Cursor.visible = true;
            }
            else
            {
                //скрыть
                Cursor.SetCursor(cursorNone, Vector2.zero, CursorMode.Auto);
                Cursor.visible = false;
                Destroy(menu);
                menu = null;
            }
        }

        //если меню открыто, то игнорируем управление персонажем
        if (menu != null)
        {
            return;
        }

        IsMoving = false;

        //Движение
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += new Vector3(0, Speed * Time.deltaTime, 0);
            IsMoving = true;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += new Vector3(0, -Speed * Time.deltaTime, 0);
            IsMoving = true;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-Speed * Time.deltaTime, 0, 0);
            IsMoving = true;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(Speed * Time.deltaTime, 0, 0);
            IsMoving = true;
        }

        //Выбор предмета
        if (Input.GetKeyDown(KeyCode.Alpha1) ||
            Input.GetKeyDown(KeyCode.Alpha2) ||
            Input.GetKeyDown(KeyCode.Alpha3) ||
            Input.GetKeyDown(KeyCode.Alpha4) ||
            Input.GetKeyDown(KeyCode.Alpha5) ||
            Input.GetKeyDown(KeyCode.Alpha6) ||
            Input.GetKeyDown(KeyCode.Alpha7) ||
            Input.GetKeyDown(KeyCode.Alpha8) ||
            Input.GetKeyDown(KeyCode.Alpha9) ||
            Input.GetKeyDown(KeyCode.Alpha0))
        {
            //изменяем выбранный предмет в руках
            ChangeSelectedItem();
        }

        //Обработчик нажатий клавиш от выбранного элемента
        if(selectedItemOnKeys != null)
        {
            selectedItemOnKeys();
        }

        //Взаимодействия с интерактивными вещами
        if (Input.GetKeyDown(KeyCode.E))
        {
            //есть интерактивный объект в радиусе действия
            if (InteractiveSelectedElement != null)
            {
                //предмет
                if (InteractiveSelectedElement.GetComponent<ItemDiscarded>())
                {
                    //добавление предмета в инвентарь
                    var itemDiscarded = InteractiveSelectedElement.GetComponent<ItemDiscarded>();
                    if (AddItem(new Item(itemDiscarded)))
                    {
                        interactiveElements.Remove(InteractiveSelectedElement); //удаляем из списка
                        Destroy(InteractiveSelectedElement); //удаляем из мира
                        InteractiveSelectedElement = null; //нету выбранного элемента

                        //изменяем выбранный предмет в руках
                        ChangeSelectedItem();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Пересечение другого колайдера с isTrigger = true
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Интерактивные элементы
        if (other.gameObject.GetComponent<InteractiveObject>())
        {
            interactiveElements.Add(other.gameObject);
        }
    }

    /// <summary>
    /// Выход из пересечения другого колайдера с isTrigger = true
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit2D(Collider2D other)
    {
        //Интерактивные элементы
        if (other.gameObject.GetComponent<InteractiveObject>())
        {
            interactiveElements.Remove(other.gameObject);
        }
    }
}
