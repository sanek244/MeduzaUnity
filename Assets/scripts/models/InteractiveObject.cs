﻿using UnityEngine;

/// <summary>
/// Интерактивный объект - с которым можно взаимодействовать игрока на определённую клавишу дейсствия
/// Используется, как тег, для пометки таких объектов 
/// </summary>
public class InteractiveObject : MonoBehaviour
{}
