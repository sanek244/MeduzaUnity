﻿using Assets.scripts;
using System.Linq;
using UnityEngine;

/// <summary>
/// Выброшенный предмет
/// </summary>
public class ItemDiscarded : UnityItem
{
    public int id; //для Unity, если задан, то модель будет загружена из бд

    public void Start()
    {
        //загрузка модели из бд
        if(id > 0)
        {
            Item item = DBController.GetInstance().connection.Table<Item>().FirstOrDefault(x => x.Id == id);
            if ((object)item != null)
            {
                Set(item);
                if(GetComponent<SpriteRenderer>() != null)
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(@"media/img/UI/items/" + item.NameCode);
                }
            }
        }
    }
}
