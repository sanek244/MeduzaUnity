﻿using UnityEngine;

[RequireComponent(typeof(AudioClip))]
public class Sound : MonoBehaviour {
	
	void Update ()
    {
        if (!gameObject.GetComponent<AudioSource>().isPlaying) {
            Destroy(gameObject);
        }
	}
}
