﻿using System.Collections.Generic;
using MeduzaServer;
using UnityEngine;

/// <summary>
/// Оружие
/// </summary>
public class Weapon : MonoBehaviour {

    /// <summary>
    /// Время последней атаки (Unix time)
    /// </summary>
    private ulong attackLastTime;
    /// <summary>
    /// Список вспышек после выстрела
    /// </summary>
    private List<GameObject> shots;

    /// <summary>
    /// Аниматоры вспышек
    /// </summary>
    private RuntimeAnimatorController[] animators;
    private GameObject bulletInit; //пуля
    private GameObject soundStrike;
    private AudioSource soundEmptyStrike;
    private AudioSource soundRecharge;

    //*** Свойства ***//
    /// <summary>
    /// Урон (для упрощения, урон не рассчитывается от скорости и характеристик снаряда, 
    ///     а задаётся самому оружию в бд и передаётся снаряду, с его надбавками, при создании)
    /// </summary>
    public int Damage { get; set; }  
    /// <summary>
    /// Скорострельность - миллисекунд между выстрелами
    /// </summary>
    public int RapidityFire { get; set; }
    /// <summary>
    /// Дальность выстрела
    /// </summary>
    public int ShotRange { get; set; }
    /// <summary>
    /// Количество патрон в магазине (максимум)
    /// </summary>
    public int StoreCapacity { get; set; }
    /// <summary>
    /// Текущее количество патрон в магазине 
    /// </summary>
    public int StoreCapacityCurrent { get; set; }
    /// <summary>
    /// Бронебойность самого оружия
    /// </summary>
    public int ArmorPiercing { get; set; }
    /// <summary>
    /// Сила отдачи при каждом выстреле
    /// </summary>
    public float RecoilForce { get; set; }
    /// <summary>
    /// Текущий уровень отдачи оружия 
    /// напрямую зависит от Scatter и времени прошедшего от предыдущего выстрела
    /// используется в Gamer для отображении на курсоре мышки и на туловище игрока
    /// </summary>
    public float RecoilCurrent { get; set; }


    //*** Методы ***//
    void Start () {
        shots = new List<GameObject>();
    }
	
	void Update ()
    {
        //Перемещение всех вспышек от выстрела (иначе выглядит не очень)
        MoveShots();

        //утихание отдачи
        if(RecoilCurrent > 0)
        {
            //RecoilCurrent -= 
        }
    }

    /// <summary>
    /// Обработка нажатий клавиш
    /// Вызывается в Gamer при выбранном оружии
    /// </summary>
    public void OnKeys()
    {
        //Нажатие левой кнопки мыши
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Strike(); //выстрел
        }
    }

    /// <summary>
    /// Инициализация по параметрам из бд
    /// </summary>
    /// <param name="item"></param>
    public void Init(Item item)
    {
        //загрузка параметров
        Damage = item.GetParameterValueInt("damage");
        ShotRange = item.GetParameterValueInt("shot_range");
        RapidityFire = item.GetParameterValueInt("rate_of_fire");
        RecoilForce = item.GetParameterValueFloat("recoil_force");
        StoreCapacity = item.GetParameterValueInt("store_capacity");
        ArmorPiercing = item.GetParameterValueInt("armor_piercing");
        string typeBullet = item.GetParameterValue("type_bullet");


        //загрузка префабов
        bulletInit = Resources.Load("prefabs/bullet/" + typeBullet) as GameObject;
        soundStrike = Resources.Load("prefabs/sound/mp5") as GameObject;
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("media/img/weapon/" + item.NameCode + "_top");
        animators = new RuntimeAnimatorController[7];
        for(int i = 0; i < animators.Length; i++)
        {
            animators[i] = Resources.Load<RuntimeAnimatorController>("media/img/shot/shot_1/shot_" + (i + 1));
        }
        //soundEmptyStrike = 
        //soundRecharge = 
    }

    /// <summary>
    /// Выстрел
    /// </summary>
    public bool Strike()
    {
        //проверка скорострельности
        if (attackLastTime + (ulong)RapidityFire > Helper.GetUnixTime()) {
            return false;
        }

        //создание пули
        var bullet = Instantiate(bulletInit);
        //var newAngle = Helper.GetScatterIncreasing(transform.eulerAngles.x, Scatter, attackLastTime);
        bullet.GetComponent<Transform>().eulerAngles = transform.eulerAngles; // new Vector3(newAngle, 0);
        bullet.GetComponent<Transform>().position = GetPositionCreateBullet();
        bullet.GetComponent<Bullet>().Damage += Damage; //урон пули
        bullet.GetComponent<Bullet>().ShotRange += ShotRange; //дальность полёта пули
        attackLastTime = Helper.GetUnixTime();

        //звук выстрела
        Instantiate(soundStrike, transform);


        //вспышка выстрела
        var shotAnimation = new GameObject();
        shotAnimation.AddComponent<Animator>();
        shotAnimation.AddComponent<SpriteRenderer>();
        shotAnimation.GetComponent<Animator>().runtimeAnimatorController = animators[Mathf.RoundToInt(Random.Range(0, animators.Length))];
        shotAnimation.transform.position = bullet.transform.position;
        shotAnimation.transform.eulerAngles = bullet.transform.eulerAngles;
        shotAnimation.name = shotAnimation.GetComponent<Animator>().runtimeAnimatorController.name;
        shots.Add(shotAnimation);

        //отдача
        RecoilCurrent += RecoilForce;

        return true;
    }

    /// <summary>
    /// Перемещение всех вспышек от выстрела (иначе выглядит не очень)
    /// </summary>
    private void MoveShots()
    {
        for (int i = 0; i < shots.Count; i++)
        {
            if (shots[i] == null)
            {
                shots.RemoveAt(i);
                i--;
            }
            else
            {
                shots[i].transform.eulerAngles = transform.eulerAngles;
                shots[i].transform.position = GetPositionCreateBullet();
            }
        }
    }

    /// <summary>
    /// Возвращает точку создания пули с учётом поворота игрока
    /// </summary>
    /// <returns></returns>
    private Vector3 GetPositionCreateBullet()
    {
        //alias
        var gamerLocation = transform.position;

        //точка появления пули (со сдвигом относительно оружия)
        var positionCreateWeapon = new Vector3(
            gamerLocation.x + 0.3f,
            gamerLocation.y,
            gamerLocation.z);

        //раастояние от оружия до точки создания пули
        float distanceGamerForWeapon = Helper.Distance(positionCreateWeapon, gamerLocation);

        return Helper.GetAngleRightTriangle(gamerLocation, transform.eulerAngles.z, distanceGamerForWeapon);
    }
}
