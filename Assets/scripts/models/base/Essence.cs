﻿using UnityEngine;
using MeduzaServer;

/// <summary>
/// Живая сущность
/// </summary>
public class Essence : MonoBehaviour, IHealth {

    //для Unity
    public int healthStart;
    public float health;
    public float armor;
    public TypeMaterial material;

    /// <summary>
    /// Анимация разрушения
    /// </summary>
    public RuntimeAnimatorController destructionAnimator;


    //*** Свойства ***//
    public int HealthStart { get { return healthStart; } set { healthStart = value; } }
    public float Health {
        get { return health; }
        set
        {
            health = value;
            if(value <= 0) {
                Destroy(gameObject);

                if(destructionAnimator != null)
                {
                    var animationDestruction = new GameObject();
                    animationDestruction.AddComponent<SpriteRenderer>();
                    animationDestruction.AddComponent<Animator>();
                    animationDestruction.GetComponent<Animator>().runtimeAnimatorController = destructionAnimator;
                    animationDestruction.name = gameObject.name + "__destructionAnimation";
                    animationDestruction.transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
                    animationDestruction.transform.position = transform.position;
                    Instantiate(animationDestruction);
                }
            }
        }
    }
    public float Armor { get { return armor; } set { armor = value; } }
    /// <summary>
    /// Материал объекта
    /// </summary>
    public TypeMaterial Material { get { return material; } set { material = value; } }
}
