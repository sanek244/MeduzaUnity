﻿using Assets.scripts.interfaces.ui;

/// <summary>
/// Категория предмета (оружие, стены)
/// </summary>
public class Category : ICategory
{
    public int Id { get; set; }
    public string NameCode { get; set; } //ключ перевода названия
    public int Sort { get; set; } //порядок сортировки при показе

    public Category() { }
    public Category(ICategory el)
    {
        Id = el.Id;
        NameCode = el.NameCode;
        Sort = el.Sort;
    }
}
