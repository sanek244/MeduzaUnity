﻿using Assets.scripts;
using Assets.scripts.interfaces.ui;
using MeduzaServer;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Предмет инвентаря и магазина
/// </summary>
public class Item : IItem
{
    public int Id { get; set; }
    public int CategoryId { get; set; } //id категории
    public int SubCategoryId { get; set; } //id подкатегории
    public int Price { get; set; } //цена
    public string NameCode { get; set; } //ключ перевода названия

    private List<Parameter> parameters; //нигде НЕ использовать, кроме функции GetParameters 

    public Item() {}
    public Item(IItem item)
    {
        Id = item.Id;
        CategoryId = item.CategoryId;
        SubCategoryId = item.SubCategoryId;
        Price = item.Price;
        NameCode = item.NameCode;
    }

    /// <summary>
    /// Возвращает параметры предмета из таблицы Parameter
    /// </summary>
    /// <returns></returns>
    public List<Parameter> GetParameters()
    {
        if(parameters == null)
        {
            parameters = DBController.GetInstance()
            .connection.Table<Parameter>()
            .Where(x => x.ItemId == Id)
            .ToList();
        }

        return parameters;
    }


    /// <summary>
    /// Возвращает значение выбранного параметра
    /// </summary>
    /// <param name="key"></param>
    /// <param name="isIgnoreError">Если нет в бд, то не создавать ошибку, а вернуть defaultValue</param>
    /// <param name="defaultValue">Значение, которое будет возвращено, если isIgnoreError = true и нет записи в бд </param>
    /// <returns></returns>
    public string GetParameterValue(string key, bool isIgnoreError = false, string defaultValue = "")
    {
        Parameter parameter = GetParameters().Where(x => x.NameCode == key).FirstOrDefault();
        if (parameter == null)
        {
            if (isIgnoreError)
            {
                return defaultValue;
            }

            Debug.LogError(string.Format("В бд отсутствует NameCode = '{0}', таблица Parameter, ItemId '{1}', ItemName '{2}'", key, Id, NameCode));
        }

        return parameter.Value;
    }
    /// <summary>
    /// Возвращает значение выбранного параметра
    /// </summary>
    /// <param name="key"></param>
    /// <param name="isIgnoreError">Если нет в бд, то не создавать ошибку, а вернуть defaultValue</param>
    /// <param name="defaultValue">Значение, которое будет возвращено, если isIgnoreError = true и нет записи в бд </param>
    /// <returns></returns>
    public float GetParameterValueFloat(string key, bool isIgnoreError = false, float defaultValue = 0)
    {
        string value = GetParameterValue(key, isIgnoreError, null);
        if(value == null)
        {
            return defaultValue;
        }
        return Helper.FloatParse(value);
    }
    /// <summary>
    /// Возвращает значение выбранного параметра
    /// </summary>
    /// <param name="key"></param>
    /// <param name="isIgnoreError">Если нет в бд, то не создавать ошибку, а вернуть defaultValue</param>
    /// <param name="defaultValue">Значение, которое будет возвращено, если isIgnoreError = true и нет записи в бд </param>
    /// <returns></returns>
    public int GetParameterValueInt(string key, bool isIgnoreError = false, int defaultValue = 0)
    {
        string value = GetParameterValue(key, isIgnoreError, null);
        if (value == null)
        {
            return defaultValue;
        }
        return int.Parse(value);
    }
}
