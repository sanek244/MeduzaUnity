﻿
/// <summary>
/// Параметр предмета
/// </summary>
public class Parameter
{
    public int Id { get; set; }
    public int ItemId { get; set; } //id предмета
    public string Value { get; set; } //значение
    public string NameCode { get; set; } //ключ перевода названия
}
