﻿
/// <summary>
/// Отображение параметра
/// </summary>
public class ParameterDisplay
{
    public string NameCode { get; set; } //название параметра
    public bool IsDisplay { get; set; } //область текста
}
