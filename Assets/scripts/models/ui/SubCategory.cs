﻿using Assets.scripts.interfaces.ui;

/// <summary>
/// Подкатегория категории
/// </summary>
public class SubCategory : ISubCategory
{
    public int Id { get; set; }
    public int ParentId { get; set; } //id категории
    public string NameCode { get; set; } //ключ перевода названия
    public int Sort { get; set; } //порядок сортировки при показе

    public SubCategory() { }
    public SubCategory(ISubCategory el)
    {
        Id = el.Id;
        ParentId = el.ParentId;
        NameCode = el.NameCode;
        Sort = el.Sort;
    }
}
