﻿using MeduzaServer;
using UnityEngine;

/// <summary>
/// Перевод текста
/// </summary>
public class TextTranslate {

    public int Id { get; set; }
    public string Area { get; set; } //область текста
    public string NameCode { get; set; } //ключ перевода
    public string RU { get; set; } //русский перевод
    public string EN { get; set; } //английский перевод
    public string ZH { get; set; } //китайский перевод

    //Конструкторы
    public TextTranslate() { }
    public TextTranslate(string NameCode)
    {
        this.NameCode = NameCode;
    }

    /// <summary>
    /// Получить текст перевода
    /// </summary>
    /// <param name="language">выбранный пользователем язык</param>
    /// <returns></returns>
    public string Text(Language language)
    {
        string res = "";

        switch (language)
        {
            case Language.RU: res = RU; break;
            case Language.ZH: res = ZH; break;
            default: res = EN; break;
        }

        return string.IsNullOrEmpty(res) ? NameCode : res;
    }
    public string Text(string languageText)
    {
        Language language = Language.EN;

        if (TypeLanguage.ValuesReverse.ContainsKey(languageText))
        {
            language = TypeLanguage.ValuesReverse[languageText];
        }

        return Text(language);
    }
    public string Text()
    {
        return Text(PlayerPrefs.GetString("language"));
    }
}
