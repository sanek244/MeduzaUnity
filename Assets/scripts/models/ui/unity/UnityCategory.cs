﻿using Assets.scripts.interfaces.ui;
using UnityEngine;

/// <summary>
/// Категория предмета (оружие, стены)
/// </summary>
public class UnityCategory : MonoBehaviour, ICategory
{
    public int Id { get; set; }
    public string NameCode { get; set; } //ключ перевода названия
    public int Sort { get; set; } //порядок сортировки при показе
}
