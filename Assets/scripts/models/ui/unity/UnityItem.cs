﻿using Assets.scripts.interfaces.ui;
using UnityEngine;

/// <summary>
/// Предмет инвентаря и магазина
/// </summary>
public class UnityItem : MonoBehaviour, IItem
{
    public int Id { get; set; }
    public int CategoryId { get; set; } //id категории
    public int SubCategoryId { get; set; } //id подкатегории
    public int Price { get; set; } //цена
    public string NameCode { get; set; } //ключ перевода названия

    /// <summary>
    /// Установка значений модели
    /// </summary>
    /// <param name="item"></param>
    public void Set(IItem item)
    {
        Id = item.Id;
        CategoryId = item.CategoryId;
        SubCategoryId = item.SubCategoryId;
        Price = item.Price;
        NameCode = item.NameCode;
    }
}
