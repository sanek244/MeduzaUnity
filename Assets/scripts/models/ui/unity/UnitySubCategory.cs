﻿using Assets.scripts.interfaces.ui;
using UnityEngine;

/// <summary>
/// Подкатегория категории
/// </summary>
public class UnitySubCategory : MonoBehaviour, ISubCategory
{
    public int Id { get; set; }
    public int ParentId { get; set; } //id категории
    public string NameCode { get; set; } //ключ перевода названия
    public int Sort { get; set; } //порядок сортировки при показе
}
